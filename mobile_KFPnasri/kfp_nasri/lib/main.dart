import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Home());
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SS',
      theme: ThemeData(fontFamily: 'iransans'),
      home: Scaffold(
          body: SafeArea(
        child: Container(
          color: Colors.black12,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Header(),
              StatusBar(),
              Panel()
            ],
          ),
        ),
      )),
    );
  }
}

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            alignment: Alignment.center,
            decoration:
            BoxDecoration(color: Colors.black12, shape: BoxShape.circle),
            child: IconButton(
              onPressed: null,
              icon: Icon(Icons.notifications, size: 25),
            ),
          ),
          Text(
            'سیستم پایش سلامت',
            style: TextStyle(fontFamily: 'lalezar', fontSize: 25),
          ),
          IconButton(
            iconSize: 40,
            onPressed: null,
            icon: CircleAvatar(
              backgroundImage: AssetImage('assets/images/Avatar.png'),
            ),
          ),
        ],
      ),
    );
  }
}

class StatusBar extends StatefulWidget {
  const StatusBar({Key? key}) : super(key: key);

  @override
  _StatusBarState createState() => _StatusBarState();
}

class _StatusBarState extends State<StatusBar> {
  var statusBigInfo, statusSmallInfo, statusChart;

  @override
  Widget build(BuildContext context) {
    statusChart = Container(
      width: 150,
      height: 150,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
    );

    statusBigInfo = Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        statusBigInfoData('امتیاز قلب', Colors.green, '32', '30'),
        statusBigInfoData('زمان فعالیت', Colors.indigo.shade500, '486', '648')
      ],
    );

    statusSmallInfo = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          statusSmallInfoData('3.024', 'کالری'),
          statusSmallInfoData('2.8', 'کیلومتر'),
          statusSmallInfoData('1.760', 'قدم')
        ]);

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: Colors.white
        ),
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            statusChart,
            Spacer(flex: 2),
            statusBigInfo,
            Spacer(),
            statusSmallInfo
          ],
        ),
      ),
    );
  }

  Widget statusBigInfoData(
      String designator, Color designatorColor, String data, String fullData) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        designator,
        style: TextStyle(
            color: designatorColor,
            // fontFamily: 'roboto',
            fontWeight: FontWeight.bold,
            fontSize: 20),
      ),
      Row(
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.alphabetic,
          children: [
            Text(data, style: TextStyle(fontSize: 35)),
            Text('/$fullData', style: TextStyle(fontSize: 20)),
          ])
    ]);
  }

  Widget statusSmallInfoData(String data, String designator) {
    return Container(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(data,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'roboto'
                )),
            Text(
              designator,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black38,
                  fontWeight: FontWeight.bold,
                  // fontFamily: 'roboto'
              ),
            )
          ]),
    );
  }
}

class Panel extends StatefulWidget {
  const Panel({Key? key}) : super(key: key);

  @override
  _PanelState createState() => _PanelState();
}

class _PanelState extends State<Panel> {

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            dataTile(
              Icons.king_bed,
              'تحلیل خواب',
              '4.5',
              'ساعت',
              'دیروز',
              Colors.deepPurple
            ),
            dataTile(
                Icons.favorite,
                'نرخ ضربان قلب',
                '72',
                'تپش \nبر دقیقه',
                '17 دفیفه پیش',
              Colors.red
            ),
          ],
        )

      ],
    ));
  }

  Widget dataTile(IconData icon, String title, String lastDataValue,
      String unit, String lastDataTime, MaterialColor color) {
    return Expanded(
      child: AspectRatio(
        aspectRatio: 1,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: color.shade50
                        ),
                        child: Icon(icon, color: color, size: 30,)),
                    Spacer(),
                    Text(lastDataValue,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                    Text(unit,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black54),),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(title,
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                      Text(lastDataTime,
                        style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black54),),
                      Spacer(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }


}

