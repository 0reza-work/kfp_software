/*******************************************************
This program was created by the
CodeWizardAVR V3.12 Advanced
Automatic Program Generator
� Copyright 1998-2014 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 7/11/2021
Author  : 
Company : 
Comments: 


Chip type               : ATmega32
Program type            : Application
AVR Core Clock frequency: 8.000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 512
*******************************************************/

#include <mega32.h>

// Declare your global variables here

// TWI functions
#include <twi.h>
#include <delay.h>

#include "lcdfont.h"
#include "logo.h"

#define OLED_WR_ADDRESS 0x3c
#define OLED_RD_ADDRESS 0x79
#define OLED_CMD  0	//Write command
#define OLED_DATA 1	//Write data

char buff[2] = {0, 0};
char OLED_GRAM[128][8];
void OLED_WR_Byte(char data, char cmd);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Refresh_Gram(void);		   
							   		    
void OLED_Init(void);
void OLED_Clear(void);
void OLED_DrawPoint(char x,char y,char t);
void OLED_Fill(char x1,char y1,char x2,char y2,char dot);
void OLED_ShowChar(char x,char y,char chr,char size,char mode);
void OLED_ShowNum(char x,char y,int num,char len,char size);
void OLED_ShowString(char x,char y,const char *p,char size);



void main(void)
{
// Declare your local variables here

// Port C initialization
// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=Out Bit1=In Bit0=In 
DDRC=(0<<DDC7) | (0<<DDC6) | (0<<DDC5) | (0<<DDC4) | (0<<DDC3) | (1<<DDC2) | (0<<DDC1) | (0<<DDC0);
// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=1 Bit1=T Bit0=T 
PORTC=(0<<PORTC7) | (0<<PORTC6) | (0<<PORTC5) | (0<<PORTC4) | (0<<PORTC3) | (1<<PORTC2) | (0<<PORTC1) | (0<<PORTC0);

twi_master_init(100);


      OLED_Init();
      //OLED_Fill(1, 1, 25, 25, 1);
    
    OLED_ShowNum(0, 0, 123, 3, 24);
    OLED_Refresh_Gram();
      
      
while (1)
      {
      // Place your code here
      

      }
}

void OLED_WR_Byte(char data, char cmd)
{
	buff[0] = (cmd == OLED_DATA) ? 0x40 : 0x80;
	buff[1] = data;
    twi_master_trans(OLED_WR_ADDRESS, buff, 2, buff, 0);
}

//Turn on the OLED display
void OLED_Display_On(void) {
    OLED_WR_Byte(0X8D, OLED_CMD);  //SET DCDC command
    OLED_WR_Byte(0X14, OLED_CMD);  //DCDC ON
    OLED_WR_Byte(0XAF, OLED_CMD);  //DISPLAY ON
}

//Turn off the OLED display
void OLED_Display_Off(void) {
    OLED_WR_Byte(0X8D, OLED_CMD);  //SET DCDC command
    OLED_WR_Byte(0X10, OLED_CMD);  //DCDC OFF
    OLED_WR_Byte(0XAE, OLED_CMD);  //DISPLAY OFF
}

//Update video memory to LCD
void OLED_Refresh_Gram(void) {
    char i, n;
    for (i = 0; i < 8; i++) {
        OLED_WR_Byte(0xb0 + i, OLED_CMD);    //Set the page address (0~7)
        OLED_WR_Byte(0x00, OLED_CMD);      //Set the display position-column low address
        OLED_WR_Byte(0x10, OLED_CMD);      //Set the display position-column high address
        for (n = 0; n < 128; n++)
            OLED_WR_Byte(OLED_GRAM[n][i], OLED_DATA);
    }
}

//Clear the screen function, after clearing the screen, the entire screen is black! It is the same as not lit!!!
void OLED_Clear(void) {
    char i, n;
    for (i = 0; i < 8; i++)
        for (n = 0; n < 128; n++)
            OLED_GRAM[n][i] = 0X00;
    OLED_Refresh_Gram();//Update display
}

//Draw some
//x:0~127
//y:0~63
//t:1 fill 0, clear
void OLED_DrawPoint(char x, char y, char t) {
    char pos, bx, temp = 0;
    if (x > 127 || y > 63)return;//Out of range.
    pos = 7 - y / 8;
    bx = y % 8;
    temp = 1 << (7 - bx);
    if (t)OLED_GRAM[x][pos] |= temp;
    else OLED_GRAM[x][pos] &= ~temp;
}

//x1,y1,x2,y2 the diagonal coordinates of the filled area
//Ensure x1<=x2;y1<=y2 0<=x1<=127 0<=y1<=63
//dot:0, clear; 1, fill
void OLED_Fill(char x1, char y1, char x2, char y2, char dot) {
    char x, y;
    for (x = x1; x <= x2; x++) {
        for (y = y1; y <= y2; y++)OLED_DrawPoint(x, y, dot);
    }
    OLED_Refresh_Gram();//Update display
}


//Display a character at the specified position, including some characters
//x:0~127
//y:0~63
//mode:0, reverse display; 1, normal display
//size: select font 16/12
void OLED_ShowChar(char x, char y, char chr, char size, char mode) {
    char temp, t, t1;
    char y0 = y;
    char csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size /
                                                    2);        //Get the number of bytes occupied by a character in the font corresponding to the dot matrix set
    chr = chr - ' ';//Get the offset value
    for (t = 0; t < csize; t++) {
        if (size == 12)temp = asc2_1206[chr][t];        //Call 1206 font
        else if (size == 16)temp = asc2_1608[chr][t];    //Call 1608 font
        else if (size == 24)temp = asc2_2412[chr][t];    //Call 2412 font
        else return;                                //No font library
        for (t1 = 0; t1 < 8; t1++) {
            if (temp & 0x80)OLED_DrawPoint(x, y, mode);
            else OLED_DrawPoint(x, y, !mode);
            temp <<= 1;
            y++;
            if ((y - y0) == size) {
                y = y0;
                x++;
                break;
            }
        }
    }
}
//m^n function
int mypow(char m, char n)
{
    int result = 1;
    while (n--)result *= m;
    return result;
}


//Display 2 numbers
//x,y: starting point coordinates
//len: the number of digits
//size: font size
//mode: mode 0, fill mode; 1, overlay mode
//num: number (0~4294967295);
void OLED_ShowNum(char x, char y, int num, char len, char size) {
    char t, temp;
    char enshow = 0;
    for (t = 0; t < len; t++) {
        temp = (num / mypow(10, len - t - 1)) % 10;
        if (enshow == 0 && t < (len - 1)) {
            if (temp == 0) {
                OLED_ShowChar(x + (size / 2) * t, y, ' ', size, 1);
                continue;
            } else enshow = 1;

        }
        OLED_ShowChar(x + (size / 2) * t, y, temp + '0', size, 1);
    }
}



//Display string
//x,y: starting point coordinates
//size: font size
//*p: string start address
void OLED_ShowString(char x, char y, const char *p, char size) {
    while ((*p <= '~') && (*p >= ' '))// Determine if it is an illegal character!
    {
        if (x > (128 - (size / 2))) {
            x = 0;
            y += size;
        }
        if (y > (64 - size)) {
            y = x = 0;
            OLED_Clear();
        }
        OLED_ShowChar(x, y, *p, size, 1);
        x += size / 2;
        p++;
    }

}


//initialization SSD1306
void OLED_Init(void)
{
	// Reset the LCD
	PORTC &= ~(1<<2);
	delay_ms(100);
    PORTC |= (1<<2);

    OLED_WR_Byte(0xAE, OLED_CMD); //Close the display                          
    OLED_WR_Byte(0xD5, OLED_CMD); //Set the clock division factor, oscillation frequency
    OLED_WR_Byte(80, OLED_CMD);   //[3:0], division factor; [7:4], oscillation frequency
    OLED_WR_Byte(0xA8, OLED_CMD); //Set the number of drive channels (Multiplex Ratio)
    OLED_WR_Byte(0X3F, OLED_CMD); //Default 0X3F(1/64)
    OLED_WR_Byte(0xD3, OLED_CMD); //Set the display offset
    OLED_WR_Byte(0X00, OLED_CMD); //The default is 0
    OLED_WR_Byte(0x40, OLED_CMD); //Set the display start line [5:0], the number of lines.
    OLED_WR_Byte(0x8D, OLED_CMD); //Charge pump settings
    OLED_WR_Byte(0x14, OLED_CMD); //bit2, turn on/off
    OLED_WR_Byte(0x20, OLED_CMD); //Set memory address mode
    //[1:0], 00, column address mode; 01, row address mode; 10, page address mode; default 10;
    OLED_WR_Byte(0x02, OLED_CMD);
    OLED_WR_Byte(0xA1, OLED_CMD); //Segment redefinition setting, bit0:0,0->0;1,0->127;
    //Set the COM scanning direction; bit3:0, normal mode; 1, redefine mode COM[N-1]->COM0;N: drive number
    OLED_WR_Byte(0xC0, OLED_CMD);
    OLED_WR_Byte(0xDA, OLED_CMD); //Set the COM hardware pin configuration
    OLED_WR_Byte(0x12, OLED_CMD); //[5:4]Configuration
    OLED_WR_Byte(0x81, OLED_CMD); //Contrast setting
    OLED_WR_Byte(0xEF, OLED_CMD); //1~255; default 0X7F (brightness setting, the larger the brighter)
    OLED_WR_Byte(0xD9, OLED_CMD); //Set the precharge period
    OLED_WR_Byte(0xf1, OLED_CMD); //[3:0],PHASE 1;[7:4],PHASE 2;
    OLED_WR_Byte(0xDB, OLED_CMD); //Set VCOMH voltage ratio
    OLED_WR_Byte(0x30, OLED_CMD); //[6:4] 000,0.65*vcc;001,0.77*vcc;011,0.83*vcc;
    OLED_WR_Byte(0xA4, OLED_CMD); //Global display is on; bit0:1, on; 0, off; (white screen/black screen)
    OLED_WR_Byte(0xA6, OLED_CMD); //Set the display mode; bit0:1, reverse display; 0, normal display
    OLED_WR_Byte(0xAF, OLED_CMD); //Turn on the display

    OLED_Clear();
}
