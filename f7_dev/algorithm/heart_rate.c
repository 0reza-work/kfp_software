/*
 * heart_rate.c
 *
 *  Created on: Sep 10, 2021
 *      Author: rezan
 */


#include "heart_rate.h"

static int32_t ir_dx[BUFFER_SIZE - MA_SIZE - 1];
static int32_t ir_x[BUFFER_SIZE - MA_SIZE];

void calculate_heart_rate(uint32_t *ir_buff,
						  uint16_t n_data,
						  uint8_t *hr_value,
						  uint8_t *value_is_valid,
						  int32_t *valleys,
						  int32_t *n_valleys){

	int32_t ir_mean = 0, k, j /* iterator */, s /* Accumulator */;
	int32_t th /* Threshlod */;
	*n_valleys = 0;


	//--Remove DC of Signal
	//----Evaluate Mean
	for(k = 0; k < n_data; ++k)
		ir_mean += ir_buff[k];
	ir_mean /= n_data;
	//----Subtract the Mean
	for(k = 0; k < n_data; ++k)
		ir_x[k] = ir_buff[k] - ir_mean;

	//--Moving Average
	for(k = 0; k < n_data - MA_SIZE; ++k){
		s = 0;
		for(j = 0; j < MA_SIZE;++j)
			s += ir_dx[k+j];
		ir_x[k] = s / MA_SIZE;
	}

	//--Calculate Difference of Data
	for(k = 0; k < n_data - MA_SIZE - 1; ++k){
		ir_dx[k] = ir_x[k+1] - ir_x[k];
	}

	//--2point MA to diff
	for(k = 0; k < n_data - MA_SIZE - 1 - 2; ++k)
		ir_dx[k] = (ir_dx[k] + ir_dx[k+1])/2;

	//--Hamming Window + Flipping
	for(k = 0; k < n_data - MA_SIZE - 1 - 2 - HAMMING_SIZE; ++k){
		s = 0;

		for(j = k; j < k + HAMMING_SIZE; ++j)
			s -= ir_dx[j] * hamm_window[j-k];
		ir_dx[k] = s / (uint32_t)1146;
	}

	//--Calculate threshold for Peak Detection
	th = 0;
	for(k = 0; k < n_data - MA_SIZE - 1 - 2 - HAMMING_SIZE; ++k)
		th += abs(ir_dx[k]);
	th /= (n_data - MA_SIZE - 1 - 2 - HAMMING_SIZE);

	//--Calculate Peak indexes

	uint8_t flag = 0;  //0: init,  1: Up, 2: down;
	int32_t	ir_max = -2147483647;
	for(k = 0; k < n_data - MA_SIZE - 1 - 2 - HAMMING_SIZE; ++k){

		if (ir_dx[k] >= th){
			if (flag == 2){
				flag = 1;
			}
			if (ir_dx[k] > ir_max && flag == 1){
				ir_max = ir_dx[k];
				valleys[*n_valleys] = k + 7;  //Store the Real Sample Number
			}
		}
		else
			if (flag == 1)  //Save the peak
			{
				flag = 2;
				(*n_valleys)++;
				ir_max = -2147483647;
			}
			else if(flag == 0)
				flag = 2;
	}

	int32_t avg_hr_samples = 0;

	for (k = 0; k < *n_valleys; ++k){  //Elimnate first and Last Maximum
		avg_hr_samples += valleys[k+1] - valleys[k];
	}

	avg_hr_samples /= (*n_valleys-1);

	if (*n_valleys > 1){
		*hr_value = (60*50) / avg_hr_samples;
		*value_is_valid = 1;
	}else
		*value_is_valid = 0;
}
