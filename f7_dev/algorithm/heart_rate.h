/*
 * heart_rate.h
 *
 *  Created on: Sep 10, 2021
 *      Author: rezan
 */

#ifndef HEART_RATE_H_
#define HEART_RATE_H_

#include "main.h"

#define true	1
#define false	0

#define MA_SIZE 4
#define HAMMING_SIZE 5

const uint16_t hamm_window[] = {41, 276, 512, 276, 41};

void calculate_heart_rate(uint32_t *ir_buff,
		  uint16_t n_data,
		  uint8_t *hr_value,
		  uint8_t *value_is_valid,
		  int32_t *valleys,
		  int32_t *n_valleys);

#endif /* HEART_RATE_H_ */
