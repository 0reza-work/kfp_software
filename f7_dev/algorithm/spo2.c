/*
 * spo2.c
 *
 *  Created on: Sep 11, 2021
 *      Author: rezan
 */

#include "spo2.h"

static int32_t ir_x[BUFFER_SIZE - MA_SIZE];
static int32_t red_x[BUFFER_SIZE - MA_SIZE];

void calculate_spo2(uint32_t *ir_buff,
		  uint32_t *red_buff,
		  uint16_t n_data,
		  uint8_t *spo2_value,
		  uint8_t *value_is_valid,
		  int32_t *valleys,
		  int32_t *n_valleys){

	int32_t ir_mean = 0, k, j /* iterator */, s_red, s_ir /* Accumulator */;

	//--Moving Average
	for(k = 0; k < n_data - MA_SIZE; ++k){
		s_red = 0;
		s_ir = 0;

		for(j = 0; j < MA_SIZE;++j)
		{
			s_ir += ir_buff[k+j];
			s_red += red_buff[k+j];
		}
			ir_x[k] = s_ir / MA_SIZE;
		red_x[k] = s_red / MA_SIZE;
	}

	//--Hamming Window + Flipping
	for(k = 0; k < n_data - MA_SIZE - HAMMING_SIZE; ++k){
		s_red = 0;
		s_ir = 0;

		for(j = k; j < k + HAMMING_SIZE; ++j)
		{
//			s_ir -= ir_x[j] * hamm_window[j-k];
//			s_red -= red_x[j] * hamm_window[j-k];
		}

		ir_x[k] = s_ir / (uint32_t)1146;
		red_x[k] = s_red / (uint32_t)1146;
	}

}
