/*
 * spo2.h
 *
 *  Created on: Sep 11, 2021
 *      Author: rezan
 */

#ifndef SPO2_H_
#define SPO2_H_

#include "main.h"
#define MA_SIZE 4
#define HAMMING_SIZE 5

//const uint16_t hamm_window[] = {41, 276, 512, 276, 41};

void calculate_heart_rate(uint32_t *ir_buff,
		  uint16_t n_data,
		  uint8_t *spo2_value,
		  uint8_t *value_is_valid,
		  int32_t *valleys,
		  int32_t *n_valleys);

#endif /* SPO2_H_ */
