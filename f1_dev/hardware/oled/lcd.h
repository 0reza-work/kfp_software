/*
 * lcd.h
 *
 *  Created on: Jun 25, 2021
 *      Author: reza
 *  Fetch from:
 *      https://github.com/MHEtLive/MH-ET-LIVE-max30102.git
 * 
 */

#ifndef LCD_H_
#define LCD_H_
#include "../../Core/Inc/main.h"

struct lcd_bmp{
	uint8_t n_rows;
	uint8_t n_columns;
	uint8_t *data;
};

#define OLED_WR_ADDRESS 0x78
#define OLED_RD_ADDRESS 0x79
#define OLED_CMD  0	//Write command
#define OLED_DATA 1	//Write data

void OLED_WR_Byte(I2C_HandleTypeDef *hi2c, uint8_t data, uint8_t cmd);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Refresh_Gram(uint8_t page_start, uint8_t page_end, uint8_t col_start, uint8_t col_end);
							   		    
void OLED_Init(void);
void OLED_Draw_Init();
void OLED_Clear(void);
void OLED_DrawPoint(uint8_t x,uint8_t y,uint8_t t);
void OLED_DrawBMP(uint8_t row_off, uint8_t col_off, struct lcd_bmp bmp, uint8_t fill);
void OLED_Fill(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2,uint8_t dot);
void OLED_ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size,uint8_t mode);
void OLED_ShowNum(uint8_t x,uint8_t y,uint32_t num,uint8_t len,uint8_t size);
void OLED_ShowString(uint8_t x,uint8_t y,const uint8_t *p,uint8_t size);	 

#endif /* LCD_H_ */
