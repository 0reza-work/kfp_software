/****************************************************************************
Font created by the LCD Vision V1.05 font & image editor/converter
(C) Copyright 2011-2013 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Font name: Trebuchet MS
Fixed font width: 11 pixels
Font height: 24 pixels
First character: 0x30
Last character: 0x39

Exported font data size:
484 bytes for displays organized as horizontal rows of bytes
334 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef OLED_ASSETS_LCD_FONT16N_H_
#define OLED_ASSETS_LCD_FONT16N_H_

#include "../lcd.h"

uint8_t _mask_16n0[] = {
		0x00, 0xE0, 0xF8, 0x1C, 0x06, 0x06, 0x06, 0x0E,
		0xFC, 0xF0, 0x00, 0x00, 0x1F, 0x7F, 0xF0, 0xC0,
		0xC0, 0xC0, 0x70, 0x7F, 0x0F, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n1[] = {
		0x00, 0x00, 0x30, 0x18, 0x1C, 0xFC, 0xFE, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n2[] = {
		0x00, 0x08, 0x0C, 0x06, 0x06, 0x06, 0x8E, 0xFC,
		0x78, 0x00, 0x00, 0x00, 0x80, 0xE0, 0xF0, 0xDC,
		0xCE, 0xC3, 0xC1, 0xC0, 0xC0, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n3[] = {
		0x00, 0x04, 0x0E, 0x06, 0x86, 0x86, 0xCE, 0x7C,
		0x38, 0x00, 0x00, 0x00, 0x40, 0xE0, 0xC0, 0xC1,
		0xC1, 0xC1, 0xE3, 0x7F, 0x3E, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n4[] = {
		0x00, 0x00, 0x00, 0x80, 0xE0, 0x70, 0x38, 0xFC,
		0xFE, 0x00, 0x00, 0x0C, 0x0E, 0x0F, 0x0D, 0x0C,
		0x0C, 0x0C, 0xFF, 0xFF, 0x0C, 0x0C, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n5[] = {
		0x00, 0x00, 0xFE, 0xFE, 0xC6, 0xC6, 0xC6, 0xC6,
		0x86, 0x00, 0x00, 0x00, 0x00, 0x41, 0xE0, 0xC0,
		0xC0, 0xC0, 0xE1, 0x7F, 0x3F, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n6[] = {
		0x00, 0x80, 0xE0, 0x70, 0xBC, 0x8C, 0x86, 0x82,
		0x00, 0x00, 0x00, 0x00, 0x1F, 0x7F, 0xE3, 0xC1,
		0xC1, 0xC1, 0xE3, 0x7F, 0x3E, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00,};

uint8_t _mask_16n7[] = {
		0x00, 0x00, 0x06, 0x06, 0x06, 0x06, 0xC6, 0xF6,
		0x7E, 0x1E, 0x06, 0x00, 0x00, 0x00, 0xC0, 0xF8,
		0x3E, 0x0F, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n8[] = {
		0x00, 0x00, 0x78, 0xFC, 0xC6, 0x86, 0xC6, 0xFC,
		0x38, 0x00, 0x00, 0x00, 0x3C, 0x7F, 0xE3, 0xC1,
		0xC1, 0xC1, 0xE3, 0x7E, 0x3C, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};

uint8_t _mask_16n9[] = {
		0x00, 0xF8, 0xFC, 0x8E, 0x06, 0x06, 0x06, 0x8E,
		0xFC, 0xF0, 0x00, 0x00, 0x00, 0x01, 0x83, 0xC3,
		0x63, 0x7B, 0x1D, 0x0F, 0x03, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00};



struct lcd_bmp font_16n[] = {
        {24, 11, _mask_16n0 },  /* 0 */
        {24, 11, _mask_16n1 },  /* 1 */
		{24, 11, _mask_16n2 },  /* 2 */
        {24, 11, _mask_16n3 },  /* 3 */
		{24, 11, _mask_16n4 },  /* 4 */
        {24, 11, _mask_16n5 },  /* 5 */
		{24, 11, _mask_16n6 },  /* 6 */
        {24, 11, _mask_16n7 },  /* 7 */
		{24, 11, _mask_16n8 },  /* 8 */
        {24, 11, _mask_16n9 },  /* 9 */
};

#endif /* OLED_ASSETS_LCD_FONT16N_H_ */
