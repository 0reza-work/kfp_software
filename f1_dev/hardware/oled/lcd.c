/*
 * lcd.c
 *
 *  Created on: Jun 25, 2021
 *      Author: reza
 *  Fetch from:
 *      https://github.com/MHEtLive/MH-ET-LIVE-max30102.git
 * 
 * 
 */

#include "lcd.h"
#include "main.h"
#include "assets/lcd_font12.h"
#include "assets/lcd_font28n.h"
#include "assets/lcd_font42n.h"
#include "assets/bitmap.h"

//OLED video memory
//The storage format is as follows.
//[0]0 1 2 3 ... 127
//[1]0 1 2 3 ... 127
//[2]0 1 2 3 ... 127
//[3]0 1 2 3 ... 127
//[4]0 1 2 3 ... 127
//[5]0 1 2 3 ... 127
//[6]0 1 2 3 ... 127
//[7]0 1 2 3 ... 127
uint8_t OLED_GRAM[128][8];

//Write a byte to SSD1306.
//hi2c: i2c instance
//data: data/command to be written
//cmd: data/command flag 0, means command; 1, means data;
void OLED_WR_Byte(I2C_HandleTypeDef *hi2c, uint8_t data, uint8_t cmd)
{
    buff[0] = (cmd == OLED_DATA) ? 0x40 : 0x00;
    buff[1] = data;
    HAL_I2C_Master_Transmit(hi2c, OLED_WR_ADDRESS, (unsigned char *)buff, 2, 1000);
}

//Turn on the OLED display
void OLED_Display_On(void)
{
    OLED_WR_Byte(&hi2c1, 0X8D, OLED_CMD); //SET DCDC command
    OLED_WR_Byte(&hi2c1, 0X14, OLED_CMD); //DCDC ON
    OLED_WR_Byte(&hi2c1, 0XAF, OLED_CMD); //DISPLAY ON
}

//Turn off the OLED display
void OLED_Display_Off(void)
{
    OLED_WR_Byte(&hi2c1, 0X8D, OLED_CMD); //SET DCDC command
    OLED_WR_Byte(&hi2c1, 0X10, OLED_CMD); //DCDC OFF
    OLED_WR_Byte(&hi2c1, 0XAE, OLED_CMD); //DISPLAY OFF
}

//Update video memory to LCD
void OLED_Refresh_Gram(uint8_t page_start, uint8_t page_end, uint8_t col_start, uint8_t col_end)
{
    uint8_t i, n;
    for (i = page_start; i <= page_end; i++)
    {
        OLED_WR_Byte(&hi2c1, i, OLED_CMD); //Set the page address (0~7)
        OLED_WR_Byte(&hi2c1, col_start, OLED_CMD);     //Set the display position-column low address
        OLED_WR_Byte(&hi2c1, col_end, OLED_CMD);     //Set the display position-column high address
        for (n = col_start; n <= col_end; n++)
            OLED_WR_Byte(&hi2c1, OLED_GRAM[n][i], OLED_DATA);
    }
}

//Clear the screen function, after clearing the screen, the entire screen is black! It is the same as not lit!!!
void OLED_Clear(void)
{
    uint8_t i, n;
    for (i = 0; i < 8; i++)
        for (n = 0; n < 128; n++)
            OLED_GRAM[n][i] = 0x00;
    OLED_Refresh_Gram(0, 7, 0, 127); //Update display
}

/* Draw some
 * x:0~127
 * y:0~63
 * t:1 fill 0, clear */
void OLED_DrawPoint(uint8_t x, uint8_t y, uint8_t t)
{
    uint8_t pos, bx, temp = 0;
    if (x > 127 || y > 63)
        return; //Out of range.
    pos = 7 - y / 8;
    bx = y % 8;
    temp = 1 << (7 - bx);
    if (t)
        OLED_GRAM[x][pos] |= temp;
    else
        OLED_GRAM[x][pos] &= ~temp;
}

void OLED_DrawBMP(uint8_t row_off, uint8_t col_off, struct lcd_bmp bmp, uint8_t fill)
{
	uint8_t i = 0, j = 0, data = 0;

	uint8_t mask = fill ? 0x01 : 0;
	for (i = 0; i < bmp.n_rows+1; ++i)
		for (j = 0; j < bmp.n_columns+1;++j)
		{
			data = bmp.data[(i/8)*8 + j];
			OLED_DrawPoint(j + col_off, i + row_off, (data >> (i%8)) & mask);
		}
}

void OLED_Draw_Init()
{

	OLED_Clear();

	/*! Draw Top Bar */
	OLED_DrawBMP(0, 93, bmpBluetooth, 1); //Bluetooth logo
	OLED_DrawBMP(0, 77, bmpCross, 1); //Bluetooth stat

	switch(eBattery)
	{
		case EMPTY:
			OLED_DrawBMP(0, 102, bmpBattery0, 1);
			break;
		case ONE:
			OLED_DrawBMP(0, 102, bmpBattery1, 1);
			break;
		case TWO:
			OLED_DrawBMP(0, 102, bmpBattery2, 1);
			break;
		case THREE:
			OLED_DrawBMP(0, 102, bmpBattery3, 1);
			break;
		default:
			OLED_DrawBMP(0, 102, bmpBattery4, 1);
			break;
	}

	/*! Draw SpO2 value */
	OLED_DrawBMP(16, 2, font_48n[FONT42N_DASH_INDEX], 1); /* First Dash */
	OLED_DrawBMP(16, 2 + 29, font_48n[FONT42N_DASH_INDEX], 1); /* Second Dash */

	/*! Draw HR value */
	OLED_DrawBMP(16, 57, bmpFullHeart, 1); /* Full Heart */
	OLED_DrawBMP(16, 71, font_28n[FONT28N_DASH_INDEX], 1); /* First Dash */
	OLED_DrawBMP(16, 71 + 19, font_28n[FONT28N_DASH_INDEX], 1); /* Second Dash */


	/*! Draw PI value */
	OLED_DrawBMP(48, 58, bmpEmptyHeart, 1); /* Empty Heart */
	OLED_DrawBMP(48, 77, font_12[FONT12_DASH_INDEX], 1); /* First Dash */
	OLED_DrawBMP(48, 77 + _char_width12[FONT28N_DASH_INDEX], font_28n[FONT28N_DASH_INDEX], 1); /* Second Dash */

}

//m^n function
uint32_t mypow(uint8_t m, uint8_t n)
{
    uint32_t result = 1;
    while (n--)
        result *= m;
    return result;
}

//initialization SSD1306
void OLED_Init(void)
{
    // Reset the LCD
    HAL_GPIO_WritePin(OLED_RES__GPIO_Port, OLED_RES__Pin, 0);
    HAL_Delay(100);
    HAL_GPIO_WritePin(OLED_RES__GPIO_Port, OLED_RES__Pin, 1);

    OLED_WR_Byte(&hi2c1, 0xAE, OLED_CMD); //Close the display
    OLED_WR_Byte(&hi2c1, 0xD5, OLED_CMD); //Set the clock division factor, oscillation frequency
    OLED_WR_Byte(&hi2c1, 80, OLED_CMD);   //[3:0], division factor; [7:4], oscillation frequency
    OLED_WR_Byte(&hi2c1, 0xA8, OLED_CMD); //Set the number of drive channels (Multiplex Ratio)
    OLED_WR_Byte(&hi2c1, 0X3F, OLED_CMD); //Default 0X3F(1/64)
    OLED_WR_Byte(&hi2c1, 0xD3, OLED_CMD); //Set the display offset
    OLED_WR_Byte(&hi2c1, 0X00, OLED_CMD); //The default is 0
    OLED_WR_Byte(&hi2c1, 0x40, OLED_CMD); //Set the display start line [5:0], the number of lines.
    OLED_WR_Byte(&hi2c1, 0x8D, OLED_CMD); //Charge pump settings
    OLED_WR_Byte(&hi2c1, 0x14, OLED_CMD); //bit2, turn on/off
    OLED_WR_Byte(&hi2c1, 0x20, OLED_CMD); //Set memory address mode
    //[1:0], 00, column address mode; 01, row address mode; 10, page address mode; default 10;
    OLED_WR_Byte(&hi2c1, 0x02, OLED_CMD);
    OLED_WR_Byte(&hi2c1, 0xA1, OLED_CMD); //Segment redefinition setting, bit0:0,0->0;1,0->127;
    //Set the COM scanning direction; bit3:0, normal mode; 1, redefine mode COM[N-1]->COM0;N: drive number
    OLED_WR_Byte(&hi2c1, 0xC0, OLED_CMD);
    OLED_WR_Byte(&hi2c1, 0xDA, OLED_CMD); //Set the COM hardware pin configuration
    OLED_WR_Byte(&hi2c1, 0x12, OLED_CMD); //[5:4]Configuration
    OLED_WR_Byte(&hi2c1, 0x81, OLED_CMD); //Contrast setting
    OLED_WR_Byte(&hi2c1, 0xEF, OLED_CMD); //1~255; default 0X7F (brightness setting, the larger the brighter)
    OLED_WR_Byte(&hi2c1, 0xD9, OLED_CMD); //Set the precharge period
    OLED_WR_Byte(&hi2c1, 0xf1, OLED_CMD); //[3:0],PHASE 1;[7:4],PHASE 2;
    OLED_WR_Byte(&hi2c1, 0xDB, OLED_CMD); //Set VCOMH voltage ratio
    OLED_WR_Byte(&hi2c1, 0x30, OLED_CMD); //[6:4] 000,0.65*vcc;001,0.77*vcc;011,0.83*vcc;
    OLED_WR_Byte(&hi2c1, 0xA4, OLED_CMD); //Global display is on; bit0:1, on; 0, off; (white screen/black screen)
    OLED_WR_Byte(&hi2c1, 0xA6, OLED_CMD); //Set the display mode; bit0:1, reverse display; 0, normal display
    OLED_WR_Byte(&hi2c1, 0xAF, OLED_CMD); //Turn on the display

    OLED_Clear();
    OLED_Draw_Init();
}
