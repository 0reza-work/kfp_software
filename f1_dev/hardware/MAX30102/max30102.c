/*
 * max30102.c
 *
 *  Created on: Jun 25, 2021
 *      Author: reza
 */

#include "main.h"
#include "max30102.h"

uint8_t i2c_buffer[2] = {0, 0};
uint32_t red_led_data_buffer[50], ir_led_data_buffer[50];

HAL_StatusTypeDef MAX30102_write(uint8_t *reg_address, uint8_t amount)
{
	return HAL_I2C_Master_Transmit(&hi2c1, MAX30102_I2C_WRITE_ADDR, reg_address, amount + 1, HAL_MAX_DELAY);
}
HAL_StatusTypeDef MAX30102_read(uint8_t reg_address, uint8_t *buffer, uint8_t amount)
{
	uint8_t radd = reg_address;
	HAL_StatusTypeDef ret;
	ret = HAL_I2C_Master_Transmit(&hi2c1, MAX30102_I2C_WRITE_ADDR, &radd, 1, HAL_MAX_DELAY);
	if (ret == HAL_OK)
	{
		ret = HAL_I2C_Master_Receive(&hi2c1, MAX30102_I2C_READ_ADDR, buffer, amount, HAL_MAX_DELAY);
	}
	return ret;
}

void MAX30102_init(void)
{
	i2c_buffer[0] = MAX30102_REG_MODE_CONFIG;
	i2c_buffer[1] = (1 << _RESET);
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_INTR_ENABLE_1;
	i2c_buffer[1] = (1<<_A_FULL)|(1<<_PPG_RDY)|(1<<_ALC_OVF);
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_INTR_ENABLE_2;
	i2c_buffer[1] = (1<<_DIE_TEMP_RDY);
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_FIFO_WR_PTR;
	i2c_buffer[1] = 0x00;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_OVF_COUNTER;
	i2c_buffer[1] = 0x00;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_FIFO_RD_PTR;
	i2c_buffer[1] = 0x00;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_FIFO_CONFIG;
	i2c_buffer[1] = _CMD_SMP_AVE__1 | _CMD_FIFO_A_FULL__17 | (1<<_FIFO_ROLLOVER_EN);
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_MODE_CONFIG;
	i2c_buffer[1] = _CMD_MODE__SPO2;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_SPO2_CONFIG;
	i2c_buffer[1] = _CMD_SPO2_ADC_RGE__16384 | _CMD_SPO2_SR__50 | _CMD_LED_PW__18;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_LED1_PA;
	i2c_buffer[1] = _CMD_LED_PA__51;
	MAX30102_write(i2c_buffer, 1);

	i2c_buffer[0] = MAX30102_REG_LED2_PA;
	i2c_buffer[1] = _CMD_LED_PA__51;
	MAX30102_write(i2c_buffer, 1);
}

uint8_t MAX30102_sweep_fifo(struct ppg_data *buffer, uint64_t *sample_idx, uint8_t adc_res)
{
	uint8_t fifo_wr_pt = 0, fifo_rd_pt = 0;

	uint8_t fifo_buff[6];

	uint8_t i = 0;

	MAX30102_read(MAX30102_REG_FIFO_WR_PTR, &fifo_wr_pt, 1);
	MAX30102_read(MAX30102_REG_FIFO_RD_PTR, &fifo_rd_pt, 1);
	fifo_wr_pt += 32;
	uint8_t number_of_data_to_read = fifo_wr_pt - fifo_rd_pt;

	if (number_of_data_to_read > 32){
		number_of_data_to_read-=32; //Cause Circular FIFO, Make rd pt. to follow wr pt.
	}

	for (i=0; i < number_of_data_to_read-1;i++){
		(buffer+i)->red = 0;
		(buffer+i)->ir = 0;
		(buffer+i)->idx = ++(*sample_idx);


		MAX30102_read(MAX30102_REG_FIFO_DATA, fifo_buff, 6);

		(buffer+i)->red = ((fifo_buff[0]& 0x03)<<16) | (fifo_buff[1] << 8) | fifo_buff[2];
		(buffer+i)->ir = ((fifo_buff[3]& 0x03)<<16) | (fifo_buff[4] << 8) | fifo_buff[5];

		(buffer+i)->red >>= 18-adc_res;
		(buffer+i)->ir >>= 18-adc_res;
	}
	return number_of_data_to_read;
}

//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
//{
//  /* Prevent unused argument(s) compilation warning */
//  char i = 0;
//	for(i=0;i<20;++i)
//	{
//		HAL_GPIO_TogglePin(LED_PC13_GPIO_Port, LED_PC13_Pin);
//		HAL_Delay(100);
//	}
//  /* NOTE: This function Should not be modified, when the callback is needed,
//           the HAL_GPIO_EXTI_Callback could be implemented in the user file
//   */
//}
//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
//	if(GPIO_Pin == GPIO_PIN_4){
//		uint8_t MAX30102_int_register[2];
//
//		MAX30102_read(MAX30102_REG_INTR_STATUS_1, MAX30102_int_register, 2);
//
//		if(MAX30102_int_register[0] & (1 << A_FULL))
//		{
//			int data_read = 0;
//			data_read = MAX30102_sweep_fifo(red_led_data_buffer, ir_led_data_buffer);
//			for(int i =0; i< data_read;++i){
////				printf("%d", (int)red_led_data_buffer[i]);
//			}
//		}
//
//		else if(MAX30102_int_register[1] & (1 << DIE_TEMP_RDY))
//			MAX30102_sweep_fifo(red_led_data_buffer, ir_led_data_buffer);
//
//	//	else if(int_register[0] & (1 << PPG_RDY ))
//	}
//
//}
//
//
void MAX30102_read_tempratue(float *die_temp)
{
	uint8_t t_int = 0;
	MAX30102_read(MAX30102_REG_TEMP_INTR, i2c_buffer, 2);
	if (i2c_buffer[0] & (1 << 7))
	{
		t_int = (i2c_buffer[0] | ((1 << 8) - 1));
		t_int = ~t_int;
	}
	else t_int = i2c_buffer[0];

	*die_temp = (float)t_int + i2c_buffer[1] * 0.0625;
}

void MAX30102_start_temprature()
{
	i2c_buffer[0] = MAX30102_REG_TEMP_CONFIG;
	i2c_buffer[1] = 0x01;
	MAX30102_write(i2c_buffer, 1);
}
