/*
 * max30102.h
 *
 *  Created on: Jun 25, 2021
 *      Author: reza
 */

#ifndef MAX30102_H_
#define MAX30102_H_



#endif /* MAX30102_H_ */

#include "main.h"

#define MAX30102_I2C_WRITE_ADDR              0xae
#define MAX30102_I2C_READ_ADDR               0xaf

//***********************// MAX30102_register address //**********************
#define MAX30102_REG_INTR_STATUS_1           0x00
#define MAX30102_REG_INTR_STATUS_2           0x01
#define MAX30102_REG_INTR_ENABLE_1           0x02
#define MAX30102_REG_INTR_ENABLE_2           0x03
#define MAX30102_REG_FIFO_WR_PTR             0x04
#define MAX30102_REG_OVF_COUNTER             0x05
#define MAX30102_REG_FIFO_RD_PTR             0x06
#define MAX30102_REG_FIFO_DATA               0x07
#define MAX30102_REG_FIFO_CONFIG             0x08
#define MAX30102_REG_MODE_CONFIG             0x09
#define MAX30102_REG_SPO2_CONFIG             0x0A
#define MAX30102_REG_LED1_PA                 0x0C
#define MAX30102_REG_LED2_PA                 0x0D
#define MAX30102_REG_MULTI_LED_CTRL1         0x11
#define MAX30102_REG_MULTI_LED_CTRL2         0x12
#define MAX30102_REG_TEMP_INTR               0x1F
#define MAX30102_REG_TEMP_FRAC               0x20
#define MAX30102_REG_TEMP_CONFIG             0x21
#define MAX30102_REG_REV_ID                  0xFE
#define MAX30102_REG_PART_ID                 0xFF

//***********************// MAX30102_Bit index //**********************
#define _A_FULL               7
#define _PPG_RDY              6
#define _ALC_OVF              5
#define _PWR_RDY              0
#define _DIE_TEMP_RDY         1

#define _SMP_AVE_2            7
#define _SMP_AVE_1            6
#define _SMP_AVE_0            5
#define _FIFO_ROLLOVER_EN     4
#define _FIFO_A_FULL_3        3
#define _FIFO_A_FULL_2        2
#define _FIFO_A_FULL_1        1
#define _FIFO_A_FULL_0        0

#define _SHDN                 7
#define _RESET                6
#define _MODE_2               2
#define _MODE_1               1
#define _MODE_0               0

#define _SPO2_ADC_RGE_1       6
#define _SPO2_ADC_RGE_0       5
#define _SPO2_SR_2            4
#define _SPO2_SR_1            3
#define _SPO2_SR_0            2
#define _LED_PW_1             1
#define _LED_PW_0             0

#define _LED1_PA_7            7
#define _LED1_PA_6            6
#define _LED1_PA_5            5
#define _LED1_PA_4            4
#define _LED1_PA_3            3
#define _LED1_PA_2            2
#define _LED1_PA_1            1
#define _LED1_PA_0            0

#define _LED2_PA_7            7
#define _LED2_PA_6            6
#define _LED2_PA_5            5
#define _LED2_PA_4            4
#define _LED2_PA_3            3
#define _LED2_PA_2            2
#define _LED2_PA_1            1
#define _LED2_PA_0            0

#define _SLOT2_2              6
#define _SLOT2_1              5
#define _SLOT2_0              4
#define _SLOT1_2              2
#define _SLOT1_1              1
#define _SLOT1_0              0

#define _SLOT4_2              6
#define _SLOT4_1              5
#define _SLOT4_0              4
#define _SLOT3_2              2
#define _SLOT3_1              1
#define _SLOT3_0              0

#define _TEMP_EN              0

//***********************// MAX30102_Register Commands //**********************
#define _CMD_SMP_AVE__1                  0x00
#define _CMD_SMP_AVE__2                  (1<<_SMP_AVE_0)
#define _CMD_SMP_AVE__4                  (1<<_SMP_AVE_1)
#define _CMD_SMP_AVE__8                  (1<<_SMP_AVE_1)|(1<<_SMP_AVE_0)
#define _CMD_SMP_AVE__16                 (1<<_SMP_AVE_2)
#define _CMD_SMP_AVE__32                 (1<<_SMP_AVE_2)|(1<<_SMP_AVE_0)

#define _CMD_FIFO_A_FULL__32             0x00
#define _CMD_FIFO_A_FULL__31             0x01
#define _CMD_FIFO_A_FULL__30             0x02
#define _CMD_FIFO_A_FULL__29             0x03
#define _CMD_FIFO_A_FULL__28             0x04
#define _CMD_FIFO_A_FULL__27             0x05
#define _CMD_FIFO_A_FULL__26             0x06
#define _CMD_FIFO_A_FULL__25             0x07
#define _CMD_FIFO_A_FULL__24             0x08
#define _CMD_FIFO_A_FULL__23             0x09
#define _CMD_FIFO_A_FULL__22             0x0a
#define _CMD_FIFO_A_FULL__21             0x0b
#define _CMD_FIFO_A_FULL__20             0x0c
#define _CMD_FIFO_A_FULL__19             0x0d
#define _CMD_FIFO_A_FULL__18             0x0e
#define _CMD_FIFO_A_FULL__17             0x0f

#define _CMD_MODE__HR                    (1<<_MODE_1)
#define _CMD_MODE__SPO2                  (1<<_MODE_1)|(1<<_MODE_0)
#define _CMD_MODE__MULTI_LED             (1<<_MODE_2)|(1<<_MODE_1)|(1<<_MODE_0)

#define _CMD_SPO2_ADC_RGE__2048          0x00
#define _CMD_SPO2_ADC_RGE__4096          (1<<_SPO2_ADC_RGE_0)
#define _CMD_SPO2_ADC_RGE__8192          (1<<_SPO2_ADC_RGE_1)
#define _CMD_SPO2_ADC_RGE__16384         (1<<_SPO2_ADC_RGE_1)|(1<<_SPO2_ADC_RGE_0)

#define _CMD_SPO2_SR__50                 0x00
#define _CMD_SPO2_SR__100                (1<<_SPO2_SR_0)
#define _CMD_SPO2_SR__200                (1<<_SPO2_SR_1)
#define _CMD_SPO2_SR__400                (1<<_SPO2_SR_1)|(1<<_SPO2_SR_0)
#define _CMD_SPO2_SR__800                (1<<_SPO2_SR_2)
#define _CMD_SPO2_SR__1000               (1<<_SPO2_SR_2)|(1<<_SPO2_SR_0)
#define _CMD_SPO2_SR__1600               (1<<_SPO2_SR_2)|(1<<_SPO2_SR_1)
#define _CMD_SPO2_SR__3200               (1<<_SPO2_SR_2)|(1<<_SPO2_SR_1)|(1<<_SPO2_SR_0)

#define _CMD_LED_PW__15                  0x00
#define _CMD_LED_PW__16                  (1<<_LED_PW_0)
#define _CMD_LED_PW__17                  (1<<_LED_PW_1)
#define _CMD_LED_PW__18                  (1<<_LED_PW_1)|(1<<_LED_PW_0)

#define _CMD_LED_PA__0                  0x00
//255 values -> Very long -> No necessary -> Never Written!
#define _CMD_LED_PA__12                 0x3f
#define _CMD_LED_PA__25                 0x7f
#define _CMD_LED_PA__51                 0xff

#define _CMD_SLOT__NONE                  0x00
#define _CMD_SLOT13__LED1_RED            (1<<_SLOT1_0)
#define _CMD_SLOT13__LED2_IR             (1<<_SLOT1_1)
#define _CMD_SLOT24__LED1_RED            (1<<_SLOT2_0)
#define _CMD_SLOT24__LED2_IR             (1<<_SLOT2_1)






extern uint8_t i2c_buffer[2];
extern uint32_t red_led_data_buffer[100], ir_led_data_buffer[100];

struct ppg_data{
	uint32_t ir;
	uint32_t red;
	uint32_t idx;
};


HAL_StatusTypeDef MAX30102_write(uint8_t* reg_address, uint8_t amount);
HAL_StatusTypeDef MAX30102_read(uint8_t reg_address, uint8_t* buffer, uint8_t amount);
void MAX30102_init(void);
uint8_t MAX30102_sweep_fifo(struct ppg_data *buffer, uint64_t *sample_idx, uint8_t adc_res);
void MAX30102_read_tempratue(float *die_temp);
void MAX30102_start_temprature();
//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);


